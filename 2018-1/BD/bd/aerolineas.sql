--
-- Name: aerolinea; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE aerolinea (
    codigo integer NOT NULL,
    nombre character varying(20),
    pais character varying(20)
);


ALTER TABLE public.aerolinea OWNER TO postgres;

--
-- Name: pasajero; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pasajero (
    pasaporte integer NOT NULL,
    nombre character varying(20),
    pais character varying(20)
);


ALTER TABLE public.pasajero OWNER TO postgres;

--
-- Name: piloto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE piloto (
    id integer NOT NULL,
    nombre character varying(20),
    codal integer
);


ALTER TABLE public.piloto OWNER TO postgres;

--
-- Name: ticket; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ticket (
    idpasajero integer NOT NULL,
    codviaje integer NOT NULL
);


ALTER TABLE public.ticket OWNER TO postgres;

--
-- Name: viaje; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE viaje (
    codigo integer NOT NULL,
    codvuelo character varying(20) NOT NULL,
    costo integer,
    fecha date,
    piloto1 integer NOT NULL,
    piloto2 integer NOT NULL
);


ALTER TABLE public.viaje OWNER TO postgres;

--
-- Name: vuelo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vuelo (
    codigo character varying(20) NOT NULL,
    codal integer NOT NULL,
    ciudadorigen character varying(20),
    ciudaddestino character varying(20),
    horasalida character varying(20),
    horallegada character varying(20)
);


ALTER TABLE public.vuelo OWNER TO postgres;

--
-- Data for Name: aerolinea; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO aerolinea VALUES (1, 'LAN', 'Chile');
INSERT INTO aerolinea VALUES (2, 'SKY', 'Chile');
INSERT INTO aerolinea VALUES (3, 'American Airlines', 'USA');
INSERT INTO aerolinea VALUES (4, 'Air France', 'France');
INSERT INTO aerolinea VALUES (5, 'Delta', 'USA');


--
-- Data for Name: pasajero; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO pasajero VALUES (171565006, 'Gonzalo Maldonado', 'Chile');
INSERT INTO pasajero VALUES (17795336, 'Stephanie London', 'Inglaterra');
INSERT INTO pasajero VALUES (70923745, 'Andrea Silva', 'Chile');
INSERT INTO pasajero VALUES (88999112, 'Juan Perez', 'Chile');
INSERT INTO pasajero VALUES (171565063, 'Felipe Mardones', 'Peru');
INSERT INTO pasajero VALUES (171568004, 'Margarita Parra', 'Argentina');
INSERT INTO pasajero VALUES (70954431, 'Luis Urrea', 'Chile');
INSERT INTO pasajero VALUES (155674003, 'Angela Cadiz', 'Chile');
INSERT INTO pasajero VALUES (80935688, 'Sebastian Jara', 'Chile');
INSERT INTO pasajero VALUES (83125688, 'Matias Navarro', 'Chile');
INSERT INTO pasajero VALUES (83125655, 'Juan', 'Chile');


--
-- Data for Name: piloto; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO piloto VALUES (1, 'Gonzalo Maldonado', 3);
INSERT INTO piloto VALUES (2, 'Stephanie London', 5);
INSERT INTO piloto VALUES (3, 'Juan Fuentes', 1);
INSERT INTO piloto VALUES (4, 'Franco Smith', 1);
INSERT INTO piloto VALUES (5, 'Felipe Mardones', 5);
INSERT INTO piloto VALUES (6, 'Margarita Parra', 3);
INSERT INTO piloto VALUES (7, 'Margarita Parra', 1);
INSERT INTO piloto VALUES (8, 'Margarita Parra', 2);
INSERT INTO piloto VALUES (9, 'Margarita Parra', 4);
INSERT INTO piloto VALUES (10, 'Margarita Parra', 5);


--
-- Data for Name: ticket; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ticket VALUES (171565006, 1);
INSERT INTO ticket VALUES (17795336, 2);
INSERT INTO ticket VALUES (171565006, 5);
INSERT INTO ticket VALUES (70923745, 1);
INSERT INTO ticket VALUES (88999112, 3);
INSERT INTO ticket VALUES (171565063, 4);
INSERT INTO ticket VALUES (171568004, 5);
INSERT INTO ticket VALUES (70954431, 2);
INSERT INTO ticket VALUES (155674003, 9);
INSERT INTO ticket VALUES (80935688, 8);
INSERT INTO ticket VALUES (83125688, 10);
INSERT INTO ticket VALUES (83125655, 7);


--
-- Data for Name: viaje; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO viaje VALUES (1, 'LA109', 55000, '2009-12-31', 5, 1);
INSERT INTO viaje VALUES (2, 'LA121', 55000, '2009-11-30', 3, 1);
INSERT INTO viaje VALUES (3, 'LA331', 75000, '2010-10-16', 4, 1);
INSERT INTO viaje VALUES (4, 'SK221', 80000, '2010-09-15', 8, 5);
INSERT INTO viaje VALUES (5, 'AA230', 27000, '2009-08-20', 9, 3);
INSERT INTO viaje VALUES (6, 'AF100', 20000, '2011-05-02', 1, 2);
INSERT INTO viaje VALUES (7, 'AF102', 155000, '2010-03-05', 2, 4);
INSERT INTO viaje VALUES (8, 'LA111', 253000, '2010-12-03', 3, 5);
INSERT INTO viaje VALUES (9, 'AA911', 33900, '2012-12-23', 5, 2);
INSERT INTO viaje VALUES (10, 'SK200', 40000, '2012-10-22', 4, 3);


--
-- Data for Name: vuelo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO vuelo VALUES ('LA109', 1, 'Santiago', 'Iquique', '22:00', '01:00');
INSERT INTO vuelo VALUES ('LA121', 1, 'Santiago', 'Iquique', '22:00', '01:00');
INSERT INTO vuelo VALUES ('LA331', 2, 'Santiago', 'Arica', '21:00', '00:00');
INSERT INTO vuelo VALUES ('SK221', 5, 'Santiago', 'Punta Arenas', '20:00', '23:00');
INSERT INTO vuelo VALUES ('AA230', 5, 'Santiago', 'Iquique', '11:00', '13:00');
INSERT INTO vuelo VALUES ('SK200', 5, 'Santiago', 'Arica', '10:00', '15:00');
INSERT INTO vuelo VALUES ('AF100', 4, 'Santiago', 'Punta Arenas', '09:00', '13:00');
INSERT INTO vuelo VALUES ('AF102', 3, 'Iquique', 'Arica', '13:00', '19:00');
INSERT INTO vuelo VALUES ('LA111', 2, 'Punta Arenas', 'Iquique', '18:00', '24:00');
INSERT INTO vuelo VALUES ('AA911', 1, 'Santiago', 'Arica', '10:00', '13:00');


--
-- Name: aerolinea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY aerolinea
    ADD CONSTRAINT aerolinea_pkey PRIMARY KEY (codigo);


--
-- Name: pasajero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pasajero
    ADD CONSTRAINT pasajero_pkey PRIMARY KEY (pasaporte);


--
-- Name: piloto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY piloto
    ADD CONSTRAINT piloto_pkey PRIMARY KEY (id);


--
-- Name: ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (idpasajero, codviaje);


--
-- Name: viaje_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY viaje
    ADD CONSTRAINT viaje_pkey PRIMARY KEY (codigo);


--
-- Name: vuelo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vuelo
    ADD CONSTRAINT vuelo_pkey PRIMARY KEY (codigo);


--
-- Name: piloto_codal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piloto
    ADD CONSTRAINT piloto_codal_fkey FOREIGN KEY (codal) REFERENCES aerolinea(codigo) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: ticket_codviaje_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_codviaje_fkey FOREIGN KEY (codviaje) REFERENCES viaje(codigo) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: ticket_idpasajero_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_idpasajero_fkey FOREIGN KEY (idpasajero) REFERENCES pasajero(pasaporte) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: viaje_codvuelo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viaje
    ADD CONSTRAINT viaje_codvuelo_fkey FOREIGN KEY (codvuelo) REFERENCES vuelo(codigo) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: viaje_piloto1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viaje
    ADD CONSTRAINT viaje_piloto1_fkey FOREIGN KEY (piloto1) REFERENCES piloto(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: viaje_piloto2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viaje
    ADD CONSTRAINT viaje_piloto2_fkey FOREIGN KEY (piloto2) REFERENCES piloto(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: vuelo_codal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vuelo
    ADD CONSTRAINT vuelo_codal_fkey FOREIGN KEY (codal) REFERENCES aerolinea(codigo) ON UPDATE CASCADE ON DELETE RESTRICT;


