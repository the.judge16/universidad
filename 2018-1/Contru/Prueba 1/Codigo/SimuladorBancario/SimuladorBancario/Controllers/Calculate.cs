﻿using SimuladorBancario.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimuladorBancario.Model;
using System.Collections;

namespace SimuladorBancario.Controllers
{
    class Calculate
    {
        public int totalSavingAccount=0;
        public int totalCtd = 0;
        public int totalCurrentAccount = 0;
        Account account;
        bool thereIsCTD;

        public Calculate()
        {
            account = (Account)(SingletonUser.getInstance()).Clone() ;
            thereIsCTD = false;

        }

        public void calculateSavingAccount(DateTime present) 
        {
            ArrayList sm = account.SavingsAccount.Deposits;
            ArrayList wi = account.SavingsAccount.Withdrawal;
            //sm = sm.OrderBy(i => i.num_of_words).ToList();
            var c = sm.Cast<Movement>().OrderBy(i => i.Date);
            var d = wi.Cast<Movement>().OrderBy(i => i.Date);
            DateTime ini = account.SavingsAccount.Created;

            while(ini <= present)
            {
                totalSavingAccount += sm.Cast<Movement>().Where(i => i.Date.Month == ini.Month && i.Date.Year == ini.Year).Select(a => a.Mount).Sum();
                totalSavingAccount -= wi.Cast<Movement>().Where(i => i.Date.Month == ini.Month && i.Date.Year == ini.Year).Select(a => a.Mount).Sum();
                totalSavingAccount = totalSavingAccount + (int)(totalSavingAccount * 0.6);
                ini = ini.AddMonths(1);
            }

        }

        public void calculateCurrentAccount(DateTime present)
        {
            DateTime ini = account.CurrentAccount.Created;
            ArrayList sm = account.CurrentAccount.Depositos;
            ArrayList wi = account.CurrentAccount.Retiros;

            var d = wi.Cast<Movement>().OrderBy(i => i.Date);
            var c = sm.Cast<Movement>().OrderBy(i => i.Date);

            while (ini <= present)
            {
                if (thereIsCTD)
                {
                    if(account.Ctd.Close.Month== ini.Month && account.Ctd.Close.Year == ini.Year)
                    {
                        totalCurrentAccount += totalCtd;
                        totalCtd = 0;
                    }
                }
                totalCurrentAccount += sm.Cast<Movement>().Where(i => i.Date.Month == ini.Month && i.Date.Year == ini.Year).Select(a => a.Mount).Sum();
                totalCurrentAccount -= wi.Cast<Movement>().Where(i => i.Date.Month == ini.Month && i.Date.Year == ini.Year).Select(a => a.Mount).Sum();



                ini = ini.AddMonths(1);
                
            }
        }

        public void calculateCTDAccount(DateTime present)
        {
            DateTime ini = account.Ctd.Created;
            bool flag = false;
            this.totalCtd = account.Ctd.Mount;
            while (ini <= present)
            {
                if (ini.Date.Month == account.Ctd.Close.Month && ini.Date.Year == account.Ctd.Close.Year)
                {
                    flag = true;
                    break;
                }
                this.totalCtd = totalCtd + (int)(totalCtd*account.Ctd.Interest);
                ini = ini.AddMonths(1);
            }
            //Si se cierra la cuenta hacer un deposito a cuenta corriente
            if (flag)
            {
               
                this.thereIsCTD = true;
            }
          
        }
    }
}
