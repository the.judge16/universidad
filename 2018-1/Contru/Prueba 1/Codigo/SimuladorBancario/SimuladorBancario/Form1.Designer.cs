﻿namespace SimuladorBancario
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimientosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depositosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cuentaDeAhorroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cuentaCorrienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retirosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cuentaDeAhorroToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cuentaCorrienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rutLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateAhorroLabel = new System.Windows.Forms.Label();
            this.dateCorrienteLabel = new System.Windows.Forms.Label();
            this.dateEdtLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label14 = new System.Windows.Forms.Label();
            this.simulateButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.selectedDateLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.EDT = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.mountAhorroLabel = new System.Windows.Forms.Label();
            this.mountCorrienteLabel = new System.Windows.Forms.Label();
            this.mountEdtLabel = new System.Windows.Forms.Label();
            this.totalMountLabel = new System.Windows.Forms.Label();
            this.cuentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarCuentaDeAhorroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarCuentaCorrienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarEDTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.dateCierreCTDLabel = new System.Windows.Forms.Label();
            this.cerrarCDTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.cuentasToolStripMenuItem,
            this.movimientosToolStripMenuItem,
            this.ayudaToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(685, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoUsuarioToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // nuevoUsuarioToolStripMenuItem
            // 
            this.nuevoUsuarioToolStripMenuItem.Name = "nuevoUsuarioToolStripMenuItem";
            this.nuevoUsuarioToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nuevoUsuarioToolStripMenuItem.Text = "Nuevo Usuario";
            this.nuevoUsuarioToolStripMenuItem.Click += new System.EventHandler(this.nuevoUsuarioToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // movimientosToolStripMenuItem
            // 
            this.movimientosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.depositosToolStripMenuItem,
            this.retirosToolStripMenuItem,
            this.cerrarCDTToolStripMenuItem});
            this.movimientosToolStripMenuItem.Name = "movimientosToolStripMenuItem";
            this.movimientosToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.movimientosToolStripMenuItem.Text = "Movimientos";
            // 
            // depositosToolStripMenuItem
            // 
            this.depositosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cuentaDeAhorroToolStripMenuItem,
            this.cuentaCorrienteToolStripMenuItem});
            this.depositosToolStripMenuItem.Name = "depositosToolStripMenuItem";
            this.depositosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.depositosToolStripMenuItem.Text = "Depositos";
            // 
            // cuentaDeAhorroToolStripMenuItem
            // 
            this.cuentaDeAhorroToolStripMenuItem.Name = "cuentaDeAhorroToolStripMenuItem";
            this.cuentaDeAhorroToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cuentaDeAhorroToolStripMenuItem.Text = "Cuenta de ahorro";
            this.cuentaDeAhorroToolStripMenuItem.Click += new System.EventHandler(this.cuentaDeAhorroToolStripMenuItem_Click);
            // 
            // cuentaCorrienteToolStripMenuItem
            // 
            this.cuentaCorrienteToolStripMenuItem.Name = "cuentaCorrienteToolStripMenuItem";
            this.cuentaCorrienteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cuentaCorrienteToolStripMenuItem.Text = "Cuenta corriente";
            this.cuentaCorrienteToolStripMenuItem.Click += new System.EventHandler(this.cuentaCorrienteToolStripMenuItem_Click);
            // 
            // retirosToolStripMenuItem
            // 
            this.retirosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cuentaDeAhorroToolStripMenuItem1,
            this.cuentaCorrienteToolStripMenuItem1});
            this.retirosToolStripMenuItem.Name = "retirosToolStripMenuItem";
            this.retirosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.retirosToolStripMenuItem.Text = "Retiros";
            // 
            // cuentaDeAhorroToolStripMenuItem1
            // 
            this.cuentaDeAhorroToolStripMenuItem1.Name = "cuentaDeAhorroToolStripMenuItem1";
            this.cuentaDeAhorroToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.cuentaDeAhorroToolStripMenuItem1.Text = "Cuenta de ahorro";
            this.cuentaDeAhorroToolStripMenuItem1.Click += new System.EventHandler(this.cuentaDeAhorroToolStripMenuItem1_Click);
            // 
            // cuentaCorrienteToolStripMenuItem1
            // 
            this.cuentaCorrienteToolStripMenuItem1.Name = "cuentaCorrienteToolStripMenuItem1";
            this.cuentaCorrienteToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.cuentaCorrienteToolStripMenuItem1.Text = "Cuenta corriente";
            this.cuentaCorrienteToolStripMenuItem1.Click += new System.EventHandler(this.cuentaCorrienteToolStripMenuItem1_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.simulateButton);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.monthCalendar1);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.rutLabel);
            this.panel1.Controls.Add(this.nameLabel);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(13, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(660, 372);
            this.panel1.TabIndex = 1;
            // 
            // rutLabel
            // 
            this.rutLabel.AutoSize = true;
            this.rutLabel.Location = new System.Drawing.Point(61, 26);
            this.rutLabel.Name = "rutLabel";
            this.rutLabel.Size = new System.Drawing.Size(25, 13);
            this.rutLabel.TabIndex = 3;
            this.rutLabel.Text = "Null";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(61, 4);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(25, 13);
            this.nameLabel.TabIndex = 2;
            this.nameLabel.Text = "Null";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "RUT: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre: ";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateAhorroLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateCorrienteLabel, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateEdtLabel, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateCierreCTDLabel, 3, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 42);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(650, 81);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tipo de cuenta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Fecha de apertura";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(165, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Cuenta de ahorro";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(327, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Cuenta corriente";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(489, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "CTD";
            // 
            // dateAhorroLabel
            // 
            this.dateAhorroLabel.AutoSize = true;
            this.dateAhorroLabel.Location = new System.Drawing.Point(165, 26);
            this.dateAhorroLabel.Name = "dateAhorroLabel";
            this.dateAhorroLabel.Size = new System.Drawing.Size(10, 13);
            this.dateAhorroLabel.TabIndex = 5;
            this.dateAhorroLabel.Text = "-";
            // 
            // dateCorrienteLabel
            // 
            this.dateCorrienteLabel.AutoSize = true;
            this.dateCorrienteLabel.Location = new System.Drawing.Point(327, 26);
            this.dateCorrienteLabel.Name = "dateCorrienteLabel";
            this.dateCorrienteLabel.Size = new System.Drawing.Size(10, 13);
            this.dateCorrienteLabel.TabIndex = 6;
            this.dateCorrienteLabel.Text = "-";
            // 
            // dateEdtLabel
            // 
            this.dateEdtLabel.AutoSize = true;
            this.dateEdtLabel.Location = new System.Drawing.Point(489, 26);
            this.dateEdtLabel.Name = "dateEdtLabel";
            this.dateEdtLabel.Size = new System.Drawing.Size(10, 13);
            this.dateEdtLabel.TabIndex = 7;
            this.dateEdtLabel.Text = "-";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Simulacion";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(16, 165);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 143);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(111, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Selecciona una fecha";
            // 
            // simulateButton
            // 
            this.simulateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simulateButton.Location = new System.Drawing.Point(582, 346);
            this.simulateButton.Name = "simulateButton";
            this.simulateButton.Size = new System.Drawing.Size(75, 23);
            this.simulateButton.TabIndex = 8;
            this.simulateButton.Text = "Simular";
            this.simulateButton.UseVisualStyleBackColor = true;
            this.simulateButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.selectedDateLabel);
            this.panel2.Location = new System.Drawing.Point(221, 165);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(436, 162);
            this.panel2.TabIndex = 9;
            // 
            // selectedDateLabel
            // 
            this.selectedDateLabel.AutoSize = true;
            this.selectedDateLabel.Location = new System.Drawing.Point(3, 0);
            this.selectedDateLabel.Name = "selectedDateLabel";
            this.selectedDateLabel.Size = new System.Drawing.Size(109, 13);
            this.selectedDateLabel.TabIndex = 0;
            this.selectedDateLabel.Text = "Fecha seleccionada: ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(118, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "-";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.EDT, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.mountAhorroLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.mountCorrienteLabel, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.mountEdtLabel, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.totalMountLabel, 1, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 17);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(427, 142);
            this.tableLayoutPanel2.TabIndex = 2;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Cuenta de ahorro";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Cuenta corriente";
            // 
            // EDT
            // 
            this.EDT.AutoSize = true;
            this.EDT.Location = new System.Drawing.Point(3, 70);
            this.EDT.Name = "EDT";
            this.EDT.Size = new System.Drawing.Size(29, 13);
            this.EDT.TabIndex = 2;
            this.EDT.Text = "CTD";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "Total";
            // 
            // mountAhorroLabel
            // 
            this.mountAhorroLabel.AutoSize = true;
            this.mountAhorroLabel.Location = new System.Drawing.Point(216, 0);
            this.mountAhorroLabel.Name = "mountAhorroLabel";
            this.mountAhorroLabel.Size = new System.Drawing.Size(13, 13);
            this.mountAhorroLabel.TabIndex = 4;
            this.mountAhorroLabel.Text = "0";
            // 
            // mountCorrienteLabel
            // 
            this.mountCorrienteLabel.AutoSize = true;
            this.mountCorrienteLabel.Location = new System.Drawing.Point(216, 35);
            this.mountCorrienteLabel.Name = "mountCorrienteLabel";
            this.mountCorrienteLabel.Size = new System.Drawing.Size(13, 13);
            this.mountCorrienteLabel.TabIndex = 5;
            this.mountCorrienteLabel.Text = "0";
            // 
            // mountEdtLabel
            // 
            this.mountEdtLabel.AutoSize = true;
            this.mountEdtLabel.Location = new System.Drawing.Point(216, 70);
            this.mountEdtLabel.Name = "mountEdtLabel";
            this.mountEdtLabel.Size = new System.Drawing.Size(13, 13);
            this.mountEdtLabel.TabIndex = 6;
            this.mountEdtLabel.Text = "0";
            // 
            // totalMountLabel
            // 
            this.totalMountLabel.AutoSize = true;
            this.totalMountLabel.Location = new System.Drawing.Point(216, 105);
            this.totalMountLabel.Name = "totalMountLabel";
            this.totalMountLabel.Size = new System.Drawing.Size(13, 13);
            this.totalMountLabel.TabIndex = 7;
            this.totalMountLabel.Text = "0";
            // 
            // cuentasToolStripMenuItem
            // 
            this.cuentasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarCuentaDeAhorroToolStripMenuItem,
            this.agregarCuentaCorrienteToolStripMenuItem,
            this.agregarEDTToolStripMenuItem});
            this.cuentasToolStripMenuItem.Name = "cuentasToolStripMenuItem";
            this.cuentasToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.cuentasToolStripMenuItem.Text = "Cuentas";
            // 
            // agregarCuentaDeAhorroToolStripMenuItem
            // 
            this.agregarCuentaDeAhorroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarToolStripMenuItem,
            this.eliminarToolStripMenuItem});
            this.agregarCuentaDeAhorroToolStripMenuItem.Name = "agregarCuentaDeAhorroToolStripMenuItem";
            this.agregarCuentaDeAhorroToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.agregarCuentaDeAhorroToolStripMenuItem.Text = "Cuenta de ahorro";
            this.agregarCuentaDeAhorroToolStripMenuItem.Click += new System.EventHandler(this.agregarCuentaDeAhorroToolStripMenuItem_Click);
            // 
            // agregarCuentaCorrienteToolStripMenuItem
            // 
            this.agregarCuentaCorrienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarToolStripMenuItem1,
            this.eliminarToolStripMenuItem1});
            this.agregarCuentaCorrienteToolStripMenuItem.Name = "agregarCuentaCorrienteToolStripMenuItem";
            this.agregarCuentaCorrienteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.agregarCuentaCorrienteToolStripMenuItem.Text = "Cuenta corriente";
            // 
            // agregarEDTToolStripMenuItem
            // 
            this.agregarEDTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agregarToolStripMenuItem2,
            this.eliminarToolStripMenuItem2});
            this.agregarEDTToolStripMenuItem.Name = "agregarEDTToolStripMenuItem";
            this.agregarEDTToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.agregarEDTToolStripMenuItem.Text = "CTD";
            // 
            // agregarToolStripMenuItem
            // 
            this.agregarToolStripMenuItem.Name = "agregarToolStripMenuItem";
            this.agregarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.agregarToolStripMenuItem.Text = "Agregar";
            this.agregarToolStripMenuItem.Click += new System.EventHandler(this.agregarToolStripMenuItem_Click);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            this.eliminarToolStripMenuItem.Click += new System.EventHandler(this.eliminarToolStripMenuItem_Click);
            // 
            // agregarToolStripMenuItem1
            // 
            this.agregarToolStripMenuItem1.Name = "agregarToolStripMenuItem1";
            this.agregarToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.agregarToolStripMenuItem1.Text = "Agregar";
            this.agregarToolStripMenuItem1.Click += new System.EventHandler(this.agregarToolStripMenuItem1_Click);
            // 
            // eliminarToolStripMenuItem1
            // 
            this.eliminarToolStripMenuItem1.Name = "eliminarToolStripMenuItem1";
            this.eliminarToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.eliminarToolStripMenuItem1.Text = "Eliminar";
            this.eliminarToolStripMenuItem1.Click += new System.EventHandler(this.eliminarToolStripMenuItem1_Click);
            // 
            // agregarToolStripMenuItem2
            // 
            this.agregarToolStripMenuItem2.Name = "agregarToolStripMenuItem2";
            this.agregarToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.agregarToolStripMenuItem2.Text = "Agregar";
            this.agregarToolStripMenuItem2.Click += new System.EventHandler(this.agregarToolStripMenuItem2_Click);
            // 
            // eliminarToolStripMenuItem2
            // 
            this.eliminarToolStripMenuItem2.Name = "eliminarToolStripMenuItem2";
            this.eliminarToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.eliminarToolStripMenuItem2.Text = "Eliminar";
            this.eliminarToolStripMenuItem2.Click += new System.EventHandler(this.eliminarToolStripMenuItem2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Fecha de cierre";
            // 
            // dateCierreCTDLabel
            // 
            this.dateCierreCTDLabel.AutoSize = true;
            this.dateCierreCTDLabel.Location = new System.Drawing.Point(489, 52);
            this.dateCierreCTDLabel.Name = "dateCierreCTDLabel";
            this.dateCierreCTDLabel.Size = new System.Drawing.Size(10, 13);
            this.dateCierreCTDLabel.TabIndex = 9;
            this.dateCierreCTDLabel.Text = "-";
            // 
            // cerrarCDTToolStripMenuItem
            // 
            this.cerrarCDTToolStripMenuItem.Name = "cerrarCDTToolStripMenuItem";
            this.cerrarCDTToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cerrarCDTToolStripMenuItem.Text = "Cerrar CDT";
            this.cerrarCDTToolStripMenuItem.Click += new System.EventHandler(this.cerrarCDTToolStripMenuItem_Click);
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.acercaDeToolStripMenuItem.Text = "Acerca de";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 412);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(701, 451);
            this.Name = "Form1";
            this.Text = "Simulador Bancario JewsRule";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label rutLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem movimientosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depositosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cuentaDeAhorroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cuentaCorrienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retirosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cuentaDeAhorroToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cuentaCorrienteToolStripMenuItem1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label dateAhorroLabel;
        private System.Windows.Forms.Label dateCorrienteLabel;
        private System.Windows.Forms.Label dateEdtLabel;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label EDT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label mountAhorroLabel;
        private System.Windows.Forms.Label mountCorrienteLabel;
        private System.Windows.Forms.Label mountEdtLabel;
        private System.Windows.Forms.Label totalMountLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label selectedDateLabel;
        private System.Windows.Forms.Button simulateButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem cuentasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarCuentaDeAhorroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarCuentaCorrienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarEDTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem agregarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label dateCierreCTDLabel;
        private System.Windows.Forms.ToolStripMenuItem cerrarCDTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
    }
}

