﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimuladorBancario.Views;
using SimuladorBancario.Model;
using SimuladorBancario.Modelo;
using SimuladorBancario.Controllers;

namespace SimuladorBancario
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            loadUser();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void nuevoUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateUser u = new CreateUser();
            u.ShowDialog();
            loadUser();


        }

        private void cuentaDeAhorroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                Account account = SingletonUser.getInstance();
                if (account.SavingsAccount != null)
                {
                    AddMount add = new AddMount(AddMount.AddOnSavingAccount);
                    add.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No existe cuenta de ahorro\nPara crear una cuenta ve a:\ncuentas->Cuenta de ahorro->agregar",
                   "Alerta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);   
                }
            }
        }

        private void cuentaCorrienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                Account account = SingletonUser.getInstance();
                if (account.CurrentAccount != null)
                {
                    AddMount add = new AddMount(AddMount.AddOnCurrentAccount);
                    add.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No existe cuenta de ahorro\nPara crear una cuenta ve a:\ncuentas->Cuenta corriente->agregar",
                   "Alerta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                }
            }
        }

        private void cuentaDeAhorroToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                Account account = SingletonUser.getInstance();
                if (account.SavingsAccount != null)
                {
                    AddMount add = new AddMount(AddMount.withdrawalsOnSavingAccount);
                    add.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No existe cuenta de ahorro\nPara crear una cuenta ve a:\ncuentas->Cuenta de ahorro->agregar",
                   "Alerta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                }
            }
        }

        private void cuentaCorrienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                Account account = SingletonUser.getInstance();
                if (account.CurrentAccount != null)
                {
                    AddMount add = new AddMount(AddMount.withdrawalsOnCurrentAccount);
                    add.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No existe cuenta de ahorro\nPara crear una cuenta ve a:\ncuentas->Cuenta corriente->agregar",
                   "Alerta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                }
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void agregarCuentaDeAhorroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                Account account = SingletonUser.getInstance();
                this.selectedDateLabel.Text = monthCalendar1.SelectionStart.ToString("dd/MM/yyyy");
                if (account.CurrentAccount!=null || account.Ctd!=null || account.SavingsAccount != null)
                {
                   Calculate c= new Calculate();
                    //this.label.Text 
                    if (account.Ctd != null)
                    {
                        c.calculateCTDAccount(monthCalendar1.SelectionStart);
                    }

                    if (account.SavingsAccount != null)
                    {
                        c.calculateSavingAccount(monthCalendar1.SelectionStart);

                    }
                    if (account.CurrentAccount != null)
                    {
                        c.calculateCurrentAccount(monthCalendar1.SelectionStart);
                    }

                    this.mountEdtLabel.Text = "$ "+c.totalCtd;
                    this.mountAhorroLabel.Text = "$ " + c.totalSavingAccount;
                    this.mountCorrienteLabel.Text = "$ " + c.totalCurrentAccount;
                    this.totalMountLabel.Text = "$ " + (c.totalCtd + c.totalSavingAccount + c.totalCurrentAccount);

                }
                else
                {
                    MessageBox.Show("No existe ninguna cuenta asociada",
                  "Alerta",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Exclamation);
                }
            }
        }


        public void loadUser()
        {
            
            if (!SingletonUser.thereIsUser())
            {
                this.nameLabel.Text = "";
                this.rutLabel.Text = "";
            }
            else
            {
                Account account = SingletonUser.getInstance();
                this.nameLabel.Text = account.Name;
                this.rutLabel.Text = account.Dni+"";
            }
            this.dateAhorroLabel.Text = "-";
            this.dateCorrienteLabel.Text = "-";
            this.dateEdtLabel.Text = "-";
            this.mountAhorroLabel.Text = "0";
            this.mountCorrienteLabel.Text = "0";
            this.mountEdtLabel.Text = "0";
            this.totalMountLabel.Text = "0";
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                CreateSavingsAccount sa = new CreateSavingsAccount();
                sa.ShowDialog();
                Account account= SingletonUser.getInstance();
                if (account.SavingsAccount != null)
                {
                    this.dateAhorroLabel.Text = account.SavingsAccount.Created.ToString("dd/MM/yyyy");

                }
                account = null;
            }
           
        }

        private void agregarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                CreateCurrentAcount ca = new CreateCurrentAcount();
                ca.ShowDialog();
                Account account = SingletonUser.getInstance();
                if (account.CurrentAccount != null)
                {
                    this.dateCorrienteLabel.Text = account.CurrentAccount.Created.ToString("dd/MM/yyyy");

                }
                account = null;
            }
           
        }

        private void agregarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                CreateCTD cctd = new CreateCTD();
                cctd.ShowDialog();
                Account account = SingletonUser.getInstance();
                if (account.Ctd != null)
                {
                    this.dateEdtLabel.Text = account.Ctd.Created.ToString("dd/MM/yyyy");

                }
                account = null;
            }
        }


        public bool thereIsAccount()
        {
            if (!SingletonUser.thereIsUser())
            {
                MessageBox.Show("No existe usuario\nPara crear cuenta ve a:\nArchivo->Nuevo Usuario",
               "Alerta",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                DialogResult result = MessageBox.Show("Estas seguro en eliminar esto?",
                  "Confirmación",
                  MessageBoxButtons.YesNo,
                  MessageBoxIcon.Question,
                  MessageBoxDefaultButton.Button2);

                if (result == DialogResult.Yes)
                {
                    Account account = SingletonUser.getInstance();
                    account.SavingsAccount = null;
                    this.dateAhorroLabel.Text = "-";
                }
               
            }
        }

        private void eliminarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                DialogResult result = MessageBox.Show("Estas seguro en eliminar esto?",
                  "Confirmación",
                  MessageBoxButtons.YesNo,
                  MessageBoxIcon.Question,
                  MessageBoxDefaultButton.Button2);

                if (result == DialogResult.Yes)
                {
                    Account account = SingletonUser.getInstance();
                    account.CurrentAccount = null;
                    
                    this.dateCorrienteLabel.Text = "-";
                }

            }
        }

        private void eliminarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                DialogResult result = MessageBox.Show("Estas seguro en eliminar esto?",
                  "Confirmación",
                  MessageBoxButtons.YesNo,
                  MessageBoxIcon.Question,
                  MessageBoxDefaultButton.Button2);

                if (result == DialogResult.Yes)
                {
                    Account account = SingletonUser.getInstance();
                    account.Ctd = null;
                    account = null;
                    this.dateEdtLabel.Text = "-";
                    this.dateCierreCTDLabel.Text = "-";
                }

            }
        }

        private void cerrarCDTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.thereIsAccount())
            {
                Account account = SingletonUser.getInstance();
                if (account.Ctd != null)
                {
                    CerrarCTD closeCtd = new CerrarCTD();
                    closeCtd.ShowDialog();

                    if (account.Ctd.IsClose)
                    {
                        this.dateCierreCTDLabel.Text = account.Ctd.Close.ToString("dd/MM/yyyy");
                    }
                }
                else
                {
                    MessageBox.Show("No existe cuenta CDT\nPara crear cuenta ve a:\nCuentas->CDT",
               "Alerta",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
                    return;
                }
                

            }
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Nombre: Patricio Quezada L.\nPrueba 1 Construccion de software\n2018",
             "Acerca de",
              MessageBoxButtons.OK,
              MessageBoxIcon.Exclamation);
           
        }
    
    }
}
