﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimuladorBancario.Model;

namespace SimuladorBancario.Modelo
{
    class Account : ICloneable
    {
        private string name;
        private int dni;
        private SavingsAccount savingsAccount;
        private CurrentAccount currentAccount;
        private CDT ctd;

        public Account(string name, int dni){
            this.name = name;
            this.dni = dni;
            this.savingsAccount = null;
            this.currentAccount = null;
            this.ctd = null;
        }

        public Account(){

        }

        public string Name { get => name; set => name = value; }
        public int Dni { get => dni; set => dni = value; }
        internal SavingsAccount SavingsAccount { get => savingsAccount; set => savingsAccount = value; }
        internal CurrentAccount CurrentAccount { get => currentAccount; set => currentAccount = value; }
        internal CDT Ctd { get => ctd; set => ctd = value; }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
