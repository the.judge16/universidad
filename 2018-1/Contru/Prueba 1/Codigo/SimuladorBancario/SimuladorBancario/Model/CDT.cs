﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorBancario.Model
{
    class CDT
    {
        private int mount;
        private double interest;
        private DateTime created;
        private DateTime close;
        private bool isClose;

        public CDT(int mount, double interest, DateTime created)
        {
            this.mount = mount;
            this.interest = interest;
            this.created = created;
            this.isClose = false;
        }

        public int Mount { get => mount; set => mount = value; }
        public double Interest { get => interest; set => interest = value; }
        public DateTime Created { get => created; set => created = value; }
        public DateTime Close { get => close; set => close = value; }
        public bool IsClose { get => isClose; set => isClose = value; }
    }
}
