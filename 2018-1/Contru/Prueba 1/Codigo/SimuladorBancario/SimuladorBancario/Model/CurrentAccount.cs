﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimuladorBancario.Model;

namespace SimuladorBancario.Modelo
{
    class CurrentAccount
    {
        private ArrayList deposits; // Depositos
        private ArrayList withdrawal; // Retitos
        private DateTime created;
       
        
        public CurrentAccount(DateTime create)
        {
            this.deposits = new ArrayList(); ;
            this.withdrawal = new ArrayList(); ;
            this.created = create;
        }

        public ArrayList Depositos { get => deposits; set => deposits = value; }
        public ArrayList Retiros { get => withdrawal; set => withdrawal = value; }
        public DateTime Created { get => created; set => created = value; }

        public void addMovement(Movement m)
        {
            this.deposits.Add(m);
        }

        public void addWithdrawal(Movement m)
        {
            this.withdrawal.Add(m);
        }
    }
}
