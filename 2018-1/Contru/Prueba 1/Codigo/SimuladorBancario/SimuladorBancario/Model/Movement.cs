﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorBancario.Model
{
    class Movement
    {
        private DateTime date;
        private int mount;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date">date of moviment</param>
        /// <param name="mount">mount of moviment</param>
        public Movement(DateTime date, int mount)
        {
            this.date = date;
            this.mount = mount;
        }

        public DateTime Date { get => date; set => date = value; }
        public int Mount { get => mount; set => mount = value; }
    }
}
