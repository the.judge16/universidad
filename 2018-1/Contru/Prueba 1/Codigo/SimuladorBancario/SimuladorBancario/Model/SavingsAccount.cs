﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimuladorBancario.Model;

namespace SimuladorBancario.Modelo
{
    class SavingsAccount
    {
        private ArrayList deposits; // Depositos
        private ArrayList withdrawal; // Retitos
        private DateTime created;

        public SavingsAccount(DateTime created)
        {
            this.deposits = new ArrayList(); ;
            this.withdrawal = new ArrayList(); ;
            this.created = created;
        }
   
        public ArrayList Deposits { get => deposits; set => deposits = value; }
        public ArrayList Withdrawal { get => withdrawal; set => withdrawal = value; }
        public DateTime Created { get => created; set => created = value; }

        /// <summary>
        /// add movement in list of deposits.
        /// </summary>
        /// <param name="m">movement to add </param>
        public void addMovement(Movement m)
        {
            this.Deposits.Add(m);
        }

        /// <summary>
        /// add movement in list of withdrawals.
        /// </summary>
        /// <param name="m">moviment to add </param>
        public void addWithdrawal(Movement m )
        {
            this.withdrawal.Add(m);
        }
    }
}
