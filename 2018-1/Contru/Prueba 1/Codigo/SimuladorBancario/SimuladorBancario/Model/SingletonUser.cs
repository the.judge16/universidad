﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimuladorBancario.Modelo;

namespace SimuladorBancario.Model
{
    class SingletonUser
    {
        private static Account acount = null;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Object type Account</returns>
        public static Account getInstance()
        {
            if (acount == null)
            {
                acount = new Account();
                return acount;
            }
            return acount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>true if Account of user exist, false if not </returns>
        public static bool thereIsUser()
        {
            if (acount == null)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Destroy current acount
        /// </summary>
        public static void Destroy()
        {
            acount = null;
        }
    }
}
