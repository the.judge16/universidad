﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimuladorBancario.Controllers;
using SimuladorBancario.Model;
using SimuladorBancario.Modelo;

namespace SimuladorBancario.Views
{
    public partial class AddMount : Form
    {
        public readonly static int AddOnSavingAccount = 0;
        public readonly static int withdrawalsOnSavingAccount = 1;
        public readonly static int AddOnCurrentAccount = 2;
        public readonly static int withdrawalsOnCurrentAccount = 3;
        private int currentState;

        public AddMount(int option)
        {
            InitializeComponent();
            this.currentState = option;
            this.setTitle();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.ToString().Equals(""))
            {
                DialogResult result = MessageBox.Show("Desea guardar cambios?",
             "Información",
             MessageBoxButtons.YesNo,
             MessageBoxIcon.Question,
             MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    if (action())
                    {
                        return;
                    }

                }
                this.Close();
            }else
            {
                MessageBox.Show("Ingrese un monto",
                  "Alerta",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Exclamation);
                return;
            }  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void setTitle()
        {
            switch (this.currentState)
            {
                case 0:
                    this.Text = "Deposito a cuenta de ahorro";
                    return;
                case 1:
                    this.Text = "Retirar dinero a cuenta de ahorro";
                    return;
                case 2:
                    this.Text = "Deposito a cuenta corriente";
                    return;
                case 3:
                    this.Text = "Retirarr dinero a cuenta corriente";
                    return;
            }
        }

        private bool action()
        {
            switch (this.currentState)
            {
                case 0:
                    return addOnSavingAccount();
                case 1:
                    return WithdrawalsOnSavingAccount();
                case 2:
                    return addOnCurrentAccount();
                case 3:
                    return WithdrawalsOnCurrentAccount();

            }
            return true;
        }

        private bool addOnSavingAccount()
        {
            if(SingletonUser.getInstance().SavingsAccount.Created.Date <= monthCalendar1.SelectionStart.Date)
            {
                
                Movement m = new Movement(monthCalendar1.SelectionStart.Date,int.Parse(textBox1.Text.ToString()));
                SingletonUser.getInstance().SavingsAccount.addMovement(m);
                return false;
            }
            else
            {
                wrongDate();
                return true;
            }
        }


        private bool WithdrawalsOnSavingAccount()
        {
            if (SingletonUser.getInstance().SavingsAccount.Created.Date <= monthCalendar1.SelectionStart.Date)
            {
                Calculate calculate = new Calculate();
                calculate.calculateSavingAccount(monthCalendar1.SelectionStart.Date);
                if(calculate.totalSavingAccount < int.Parse(textBox1.Text.ToString()))
                {
                     MessageBox.Show("El monto a retirar, excede el total de la cuenta.",
                    "Alerta",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Exclamation);
                    return true;
                }
                else
                {
                    Movement movement = new Movement(monthCalendar1.SelectionStart.Date, int.Parse(textBox1.Text.ToString()));
                    SingletonUser.getInstance().SavingsAccount.addWithdrawal(movement);
                }
                return false;
            }
            else
            {
                wrongDate();
                return true;
            }
        }

        private bool addOnCurrentAccount()
        {
            if (SingletonUser.getInstance().CurrentAccount.Created.Date <= monthCalendar1.SelectionStart.Date)
            {
                Movement m = new Movement(monthCalendar1.SelectionStart.Date, int.Parse(textBox1.Text.ToString()));
                SingletonUser.getInstance().CurrentAccount.addMovement(m);
                return false;
            }
            else
            {
                wrongDate();
                return true;
            }
        }

        private bool WithdrawalsOnCurrentAccount()
        {
            if (SingletonUser.getInstance().CurrentAccount.Created.Date <= monthCalendar1.SelectionStart.Date)
            {
                Calculate calculate = new Calculate();
                calculate.calculateCTDAccount(monthCalendar1.SelectionStart.Date);
                calculate.calculateCurrentAccount(monthCalendar1.SelectionStart.Date);
                if (calculate.totalCurrentAccount < int.Parse(textBox1.Text.ToString()))
                {
                    MessageBox.Show("El monto a retirar, excede el total de la cuenta.",
                   "Alerta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                    return true;
                }
                else
                {
                    Movement movement = new Movement(monthCalendar1.SelectionStart.Date, int.Parse(textBox1.Text.ToString()));
                    SingletonUser.getInstance().CurrentAccount.addWithdrawal(movement);
                }
                return false;
            }
            else
            {
                wrongDate();
                return true;
            }
        }

        private void wrongDate()
        {
            MessageBox.Show("La fecha seleccionada es inferior a la fecha creada de la cuenta",
                 "Alerta",
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Exclamation);
        }

        



        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
