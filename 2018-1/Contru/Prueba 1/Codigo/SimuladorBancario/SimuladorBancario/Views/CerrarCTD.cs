﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimuladorBancario.Model;
using SimuladorBancario.Modelo;

namespace SimuladorBancario.Views
{
    public partial class CerrarCTD : Form
    {
        public CerrarCTD()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Desea guardar cambios?",
             "Información",
             MessageBoxButtons.YesNo,
             MessageBoxIcon.Question,
             MessageBoxDefaultButton.Button2);
            if (result == DialogResult.Yes)
            {
                Account account = SingletonUser.getInstance();
                //CDT cdt = new CDT(mount, interest, this.monthCalendar1.SelectionStart);
                if(account.Ctd.Created.Date > this.monthCalendar1.SelectionStart)
                {
                    MessageBox.Show("La fecha ingresada es menor a la fecha de creación",
               "Alerta",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
                    return;
                }
                else
                {
                    account.Ctd.Close = this.monthCalendar1.SelectionStart;
                    account.Ctd.IsClose = true;
                }
                

            }
            this.Close();
        }
    }
}
