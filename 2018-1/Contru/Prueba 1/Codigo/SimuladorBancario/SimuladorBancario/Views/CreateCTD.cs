﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimuladorBancario.Model;
using SimuladorBancario.Modelo;

namespace SimuladorBancario.Views
{
    public partial class CreateCTD : Form
    {
        public CreateCTD()
        {
            InitializeComponent();
        }

        //TODO: validar ambos textBox
        private void button2_Click(object sender, EventArgs e)
        {
            if(textBox1.Text.ToString().Equals(""))
            {
                MessageBox.Show("No se puede realizar la accción",
                 "Campo interés vacio",
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                return;
            }
            if (textBox2.Text.ToString().Equals(""))
            {
                MessageBox.Show("No se puede realizar la accción",
                "Campo monto vacio",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
                return;
            }

            double interest = double.Parse(textBox1.Text.ToString());
            int mount = int.Parse(textBox2.Text.ToString());

            DialogResult result = MessageBox.Show("Desea guardar cambios?",
             "Información",
             MessageBoxButtons.YesNo,
             MessageBoxIcon.Question,
             MessageBoxDefaultButton.Button2);
            if (result == DialogResult.Yes)
            {
                Account account = SingletonUser.getInstance();
                CDT cdt = new CDT(mount,interest,this.monthCalendar1.SelectionStart);
                account.Ctd = cdt;

            }
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
