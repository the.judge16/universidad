﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimuladorBancario.Model;
using SimuladorBancario.Modelo;

namespace SimuladorBancario.Views
{
    public partial class CreateCurrentAcount : Form
    {
        public CreateCurrentAcount()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Desea guardar cambios?",
              "Información",
              MessageBoxButtons.YesNo,
              MessageBoxIcon.Question,
              MessageBoxDefaultButton.Button2);
            if (result == DialogResult.Yes)
            {
                Account account = SingletonUser.getInstance();
                CurrentAccount ca = new CurrentAccount(this.monthCalendar1.SelectionStart);
                account.CurrentAccount = ca;

            }
            this.Close();
        }
    }
}
