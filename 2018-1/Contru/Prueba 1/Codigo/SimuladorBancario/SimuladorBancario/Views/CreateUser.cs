﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimuladorBancario.Model;
using SimuladorBancario.Modelo;

namespace SimuladorBancario.Views
{
    public partial class CreateUser : Form
    {
        public CreateUser()
        {
            InitializeComponent();
        }


        //Todo:Validar textbox rut paste
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.ToString().Equals(""))
            {
                MessageBox.Show("No se puede realizar la accción",
                 "Campo nombre vacio",
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                return;
            }
            if (textBox2.Text.ToString().Equals(""))
            {
                MessageBox.Show("No se puede realizar la accción",
                 "Campo nombre vacio",
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Exclamation);
                return;
            }

            DialogResult result = MessageBox.Show("Desea guardar cambios?",
               "Información",
               MessageBoxButtons.YesNo,
               MessageBoxIcon.Question,
               MessageBoxDefaultButton.Button2);
            if(result == DialogResult.Yes)
            {
                SingletonUser.Destroy();
                Account acount = SingletonUser.getInstance();
                acount.Name = textBox1.Text.ToString();
                acount.Dni = int.Parse(textBox2.Text.ToString());
            }
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
