!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Curico
!
!
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.3
 encapsulation dot1Q 3
 ip address 172.17.0.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.4
 encapsulation dot1Q 4
 ip address 172.17.32.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.5
 encapsulation dot1Q 5
 ip address 172.17.64.1 255.255.224.0
 ip helper-address 172.17.0.2
 ip access-group VLAN_5_7_9 out
!
interface FastEthernet0/0.6
 encapsulation dot1Q 6
 ip address 172.17.96.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.7
 encapsulation dot1Q 7
 ip address 172.17.128.1 255.255.224.0
 ip helper-address 172.17.0.2
 ip access-group VLAN_5_7_9 out
!
interface FastEthernet0/0.8
 encapsulation dot1Q 8
 ip address 172.17.160.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.9
 encapsulation dot1Q 9
 ip address 172.17.192.1 255.255.224.0
 ip helper-address 172.17.0.2
 ip access-group VLAN_5_7_9 out
!
interface FastEthernet0/0.10
 encapsulation dot1Q 10
 ip address 172.17.224.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/1/0
 ip address 10.0.0.17 255.255.255.248
 ip access-group 100 out
 clock rate 64000
!
interface Serial0/3/0
 ip address 10.0.0.10 255.255.255.248
 ip access-group 100 out
 clock rate 64000
!
interface Vlan1
 no ip address
 shutdown
!
router eigrp 1
 redistribute ospf 1 metric 10000 100 255 1 1500 
 network 10.0.0.0
 no auto-summary
!
router ospf 1
 log-adjacency-changes
 redistribute eigrp 1 subnets 
 network 10.0.0.8 0.0.0.7 area 0
 network 172.17.32.0 0.0.31.255 area 1
 network 172.17.64.0 0.0.31.255 area 1
 network 172.17.96.0 0.0.31.255 area 1
 network 172.17.128.0 0.0.31.255 area 1
 network 172.17.160.0 0.0.31.255 area 1
 network 172.17.192.0 0.0.31.255 area 1
 network 172.17.224.0 0.0.31.255 area 1
 network 172.17.0.0 0.0.31.255 area 1
!
ip classless
!
ip flow-export version 9
!
!
ip access-list standard VLAN_5_7_9
 remark ACL para con VLAN de invitados. Pueden acceder solo a DNS,DHCP Y icc.cl
 permit host 172.17.0.2
 permit host 172.17.0.3
 permit host 172.17.0.4
 deny any
!
banner motd 
***********************************************
***********     Casa central      *************
***********************************************

!
!
!
!
telephony-service
 max-ephones 28
 max-dn 30
 ip source-address 172.17.224.1 port 2000
 auto assign 1 to 25
!
ephone-dn 1
 number 54001
!
ephone-dn 2
 number 102
!
ephone 1
 device-security-mode none
 type 7960
!
ephone 2
 device-security-mode none
 mac-address 0006.3E31.0E71
 type 7960
 button 1:2
!
line con 0
 password cisco
 login
!
line aux 0
!
line vty 0 4
 password cisco
 login
!
!
!
end

