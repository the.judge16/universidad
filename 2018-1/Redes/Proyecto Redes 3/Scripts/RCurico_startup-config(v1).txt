!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Curico
!
!
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.3
 encapsulation dot1Q 3
 ip address 172.17.0.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.4
 encapsulation dot1Q 4
 ip address 172.17.32.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.5
 encapsulation dot1Q 5
 ip address 172.17.64.1 255.255.224.0
 ip helper-address 172.17.0.2
 ip access-group VLAN_5_7_9 out
!
interface FastEthernet0/0.6
 encapsulation dot1Q 6
 ip address 172.17.96.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.7
 encapsulation dot1Q 7
 ip address 172.17.128.1 255.255.224.0
 ip helper-address 172.17.0.2
 ip access-group VLAN_5_7_9 out
!
interface FastEthernet0/0.8
 encapsulation dot1Q 8
 ip address 172.17.160.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/0.9
 encapsulation dot1Q 9
 ip address 172.17.192.1 255.255.224.0
 ip helper-address 172.17.0.2
 ip access-group VLAN_5_7_9 out
!
interface FastEthernet0/0.10
 encapsulation dot1Q 10
 ip address 172.17.224.1 255.255.224.0
 ip helper-address 172.17.0.2
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/1/0
 ip address 10.0.0.17 255.255.255.248
 clock rate 2000000
!
interface Serial0/3/0
 ip address 10.0.0.10 255.255.255.248
 clock rate 2000000
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
ip access-list standard VLAN_5_7_9
 remark ACL para con VLAN de invitados. Pueden acceder solo a DNS,DHCP Y icc.cl
 permit host 172.17.0.2
 permit host 172.17.0.3
 permit host 172.17.0.4
 deny any
!
banner motd 
***********************************************
***********     Casa central      *************
***********************************************

!
!
!
!
telephony-service
 max-ephones 16
 max-dn 16
 ip source-address 172.17.224.1 port 2000
 auto assign 1 to 40
!
ephone-dn 1
 number 4000
!
ephone-dn 2
 number 4001
!
ephone-dn 3
 number 4003
!
ephone-dn 4
 number 4004
!
ephone-dn 5
 number 4005
!
ephone-dn 6
 number 4006
!
ephone-dn 7
 number 4007
!
ephone-dn 8
 number 4008
!
ephone-dn 9
 number 4009
!
ephone-dn 10
 number 4010
!
ephone-dn 11
 number 4011
!
ephone-dn 12
 number 4012
!
ephone-dn 13
 number 4013
!
ephone-dn 14
 number 4014
!
ephone-dn 15
 number 4015
!
ephone-dn 16
 number 4016
!
ephone 1
 device-security-mode none
!
line con 0
 password cisco
 login
!
line aux 0
!
line vty 0 4
 password cisco
 login
!
!
!
end

