/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

/**
 *
 * @author patin
 */
public class Functions {
    
    public static String ConvertIntToBit(int n, int maxLenght){
       //System.out.print("Numro: "+ n+" ");
        String bit = Integer.toBinaryString(n);
       // System.out.print("Largo: "+bit.length() + " "+bit+" " );
        while(bit.length()<maxLenght){
            bit="0"+bit;
        }
        //System.out.println("Convert to int "+bit.length());
        return bit;
    }
    
    public static String convertCharToBinary(char x){
        String bit = Integer.toBinaryString(x);
        return bit;
    }
    
     public static String FillStringToBit(String n, int maxLenght){
        String bit = new StringBuilder(n).reverse().toString();
        while(bit.length()<maxLenght){
            bit="0"+bit;
        }
        return bit;
    }
     
     public static int convertBinaryToInt(String s){
         return Integer.parseInt(s, 2);
     }
     
     public static byte[] convertStringToByte(String a){
         
         byte[] bytes = new byte[a.length()];
         for(int i = 0 ; i < bytes.length ; ++i) {
            bytes[i] = Byte.parseByte(a.charAt(i)+"");
         }
         
         return  bytes;
     }
}
