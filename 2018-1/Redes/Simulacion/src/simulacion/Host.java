/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author patin
 */
public class Host {
    private String ip;
    private String mac;
    private int port;
    private Queue<String> buffer;
    private ArrayList<Bucket> file;

    public Host(String ip,String mac){
        this.buffer = new LinkedList<>();
        this.ip=ip;
        this.mac=mac;
        this.file = new ArrayList<>();
    }
    
    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }
    
    public void addPackage(String m){
        this.buffer.add(m);
    }

    public Queue<String> getBuffer() {
        return buffer;
    }

    public void setBuffer(Queue<String> buffer) {
        this.buffer = buffer;
    }

    public ArrayList<Bucket> getFile() {
        return file;
    }

    public void setFile(ArrayList<Bucket> file) {
        this.file = file;
    }

    public void decode(){
        if(!this.buffer.isEmpty()){
            String m = this.buffer.peek();
            System.out.println(this.getIp()+", You have a menssage from "+Functions.convertBinaryToInt(m.substring(14 , 22)) +"."+Functions.convertBinaryToInt(m.substring(22 , 30))+"."+Functions.convertBinaryToInt(m.substring(30 , 38))+"."+Functions.convertBinaryToInt(m.substring(38 , 46)));
            System.out.println("And it have "+ this.buffer.size() +" Package(s)");
//this.BuildString();
        }
    }
    
    private void BuildString(){
       String message="";
       String aux="";
       String a;
       int cantidad;
       while(!this.buffer.isEmpty()){
           aux = this.buffer.poll();
           cantidad = Functions.convertBinaryToInt(aux.substring(90, 106));
           //System.out.println(  cantidad);
           
           /*for (int i = 106 + (170-106-cantidad); i < 170; i=i+8) {
               a=new StringBuilder(aux.substring(i, i+8)).reverse().toString();
               message=message+ new String(Functions.convertStringToByte(a), StandardCharsets.UTF_8);
           }*/
           
           
       }
       //new String(message, "UTF-8");
        System.out.println(message);
        
    }
    
    
    
}
