/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author patin
 */
public class Message {
        
    Host pc1;
    Host pc2;
    String message;
    Queue<String> auxMessage;
    private int segment;
    int port;
    ArrayList<String> packag;

    public Message(Host pc1, Host pc2, String Message) {
        this.pc1 = pc1;
        this.pc2 = pc2;
        this.message = Message;
        this.auxMessage=new LinkedList<>();
        this.segment=170;
        this.port=1;
        packag= new ArrayList<>();
    }
    
    /**
     * Primero
     */
    public void convertMessage(){
        for (int i = 0; i < message.length(); i++) {
            
            //System.out.println(Functions.convertCharToBinary(message.charAt(i)));
            auxMessage.add(Functions.convertCharToBinary(message.charAt(i)));
        }
          //System.out.println("tiene "+auxMessage.size());
    }
    
    /*
        Segundo
    */
    public void tcpEncapsulation(){
        String tcp="";
        int max=getMaxSegment();
        //System.out.println("maximo´"+max);
        String newport = Functions.ConvertIntToBit(this.port, 4);
        int secuence=0;
        int elements=0;
        String element="";
        //System.out.println(element.length());
        //ArrayList<String> packag= new ArrayList<>();

        while(!this.auxMessage.isEmpty()){
            
            if(element.length()==0){
              element=auxMessage.poll();
            }
            while(element.length()!=0) {
                if(elements == max){ 
                    break;
                }
                tcp=element.charAt(0)+tcp;
                //System.out.println(tcp);
                element= element.substring(1);
                //System.out.println(element);
                ++elements;
            }
            if(elements==max){
                //System.out.println("port: "+newport.length()+" S"+Functions.ConvertIntToBit(secuence, 16).length() + " CANT"+Functions.ConvertIntToBit(elements, 8).length());
                tcp = newport + Functions.ConvertIntToBit(secuence, 16) + Functions.ConvertIntToBit(elements, 8)+tcp;
                //System.out.println("tcp:\n"+tcp.length());
                packag.add(tcp);
                tcp="";
                ++secuence;
                elements=0;
            }
            
   
        }
        //System.out.println();
        if(tcp.length()!=0){
            //System.out.println("port: "+newport.length()+" S"+Functions.ConvertIntToBit(secuence, 16).length() + " CANT"+Functions.ConvertIntToBit(elements, 8).length());

            //System.out.println("tcp2: \n"+Functions.FillStringToBit(tcp, max).length());
            tcp= newport+Functions.ConvertIntToBit(secuence, 16) + Functions.ConvertIntToBit(tcp.length()-1, 8)+ Functions.FillStringToBit(tcp, max);
            //System.out.println("tcp2: "+tcp.length());
            packag.add(tcp);
        }
      
        //System.out.println(packag.size());
        
    }
    
    /**
     * Tercero
     */
    public void putRed(){
        //System.out.println(pc1.getIp() + " " + pc2.getIp());
        String[] root = (pc1.getIp()).split("\\.");
        String[] destiny = (pc2.getIp()).split("\\.");
        //System.out.println(root.length);
        //System.out.println(destiny.length);
        String o;
        for (int i = 0; i < this.packag.size(); i++) {
            o=this.packag.get(i);
            o = Functions.ConvertIntToBit(Integer.parseInt(root[0]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(root[1]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(root[2]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(root[3]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(destiny[0]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(destiny[1]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(destiny[2]),8)+
               Functions.ConvertIntToBit(Integer.parseInt(destiny[3]),8)+o;
            this.packag.set(i, o);
            //System.out.println("red: "+o.length());
        }
    }
    
    /**
     * 4to
     */
     public void physicAddress(){
        //System.out.println(pc1.getIp() + " " + pc2.getIp());
        String root = pc1.getMac();
        String destiny = pc2.getMac();
        //System.out.println(root.length);
        //System.out.println(destiny.length);
        String o;
        for (int i = 0; i < this.packag.size(); i++) {
            o=this.packag.get(i);
            o = Functions.ConvertIntToBit(Integer.parseInt(destiny), 7)+Functions.ConvertIntToBit(Integer.parseInt(root), 7)+o;
            //System.out.println("phisic "+o.length());
            this.packag.set(i, o);
        }
    }
    
    
    
    public void printArray(){
        System.out.println("------------------------------------");
        for (String o : this.packag) {
            System.out.println(o);
        }
    }
    
   public int getSegment() {
        return segment;
    }

    public void setSegment(int segment) {
        this.segment = segment;
    }
    
    public int getMaxSegment(){
        return segment-106;
    }
    
    public void prepareToSend(){
        //System.out.println("........................................................");
        this.convertMessage();
        this.tcpEncapsulation();
        //this.printArray();
        this.putRed();
        //this.printArray();
        this.physicAddress();
        //this.printArray();
        //System.out.println("largo del paquete"+);
        System.out.println("Loading package for send "+pc1.getIp()+" To "+pc2.getIp());
        for (String string : this.packag) {
            this.auxMessage.add(string);
        }
        System.out.println(""+this.auxMessage.size()+" package(s) is ready for sent");
    }
    
    public boolean packageInQueue(){
        if(this.auxMessage.isEmpty()){
            return false;
        }
        else{
            return true;
        }
    }
    
    public String sendPackage(){
        return this.auxMessage.poll();
    }
    
    
}
