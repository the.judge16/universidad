/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

import java.util.ArrayList;

/**
 *
 * @author patin
 */
public class Red {
    ArrayList<Host> hosts;
    Switch s;
    
    public Red(){
        this.hosts = new ArrayList<>();
    }

    public Switch getS() {
        return s;
    }

    public void setS(Switch s) {
        this.s = s;
    }
    
    public void addHost(Host h){
        System.out.println(h.getMac()+" is attached to the network");
        this.hosts.add(h);
    }
    
     public void sendPack(){
         Host aux=null;
         String m;
         while(!this.getS().buffer.isEmpty()){
             //System.out.println(Functions.convertBinaryToInt(this.getS().buffer.poll().substring(0, 7)));
             m=this.getS().buffer.poll();
             aux = getHost(Functions.convertBinaryToInt(m.substring(0, 7)));
             //System.out.println("aux "+aux.getMac());
             if(aux!=null){
                 aux.addPackage(m);
                 aux=null;
             }
         }
        
    }
    
     /**
      * 
      * @param i
      * @return Host that is sent the mensage
      */
    public Host getHost(int i){
        for (Host host : hosts) {
            if(host.getMac().equals(i+"")){
                return host;
            }
        }
        return null;
    }
    
}
