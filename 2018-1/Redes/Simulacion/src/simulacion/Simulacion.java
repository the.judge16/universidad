/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

/**
 *
 * @author patin
 */
public class Simulacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //System.out.println(Functions.ConvertIntToBit(98, 8));
        Host p = new Host("192.168.1.12", "12");
        Host q = new Host("192.168.1.123", "13");
        Host r = new Host("192.168.1.100", "15");
        
        Red red = new Red();
        Switch s = new Switch();
        red.setS(s);
        
       red.addHost(p);
       red.addHost(q);
       red.addHost(r);
       System.out.println("");
       
       Message n = new Message(p, q, "Esto es un mensaje");
       n.prepareToSend();
       
       Message m = new Message(q, r, "La cancion se llamaba paranoid android");
       m.prepareToSend();
        
        
       while(n.packageInQueue() || m.packageInQueue()){
           if(n.packageInQueue()){
               red.getS().addPackaje(n.sendPackage());
           }
           
            if(m.packageInQueue()){
               red.getS().addPackaje(m.sendPackage());
           }
       } 
      
        red.sendPack();
        //System.out.println("TOTAL "+red.getS().buffer.size());
        //System.out.println("----------------- Cantidad de paquetes devueltos------------------------");
        p.decode();
        q.decode();
        r.decode();
        //System.out.println(p.getBuffer().size());
        //System.out.println(q.getBuffer().size());
        //System.out.println(r.getBuffer().size());

    }
    
}
