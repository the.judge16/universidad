%{
/*
 * Autor:  Rodrigo Paredes Moraleda
 * correo: raparede@dcc.uchile.cl
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
int tabla[3];
void setear_valor(char, int);
int buscar_valor(char);
%}

%%
lineas	: linea lineas
	|
	;
linea	: expr '\n'			{ printf("%d\n-> ", $1); }
	| variable '=' expr '\n'	{ setear_valor($1, $3);
					  printf("%c asignado con %d\n-> ", $1, $3); }
	| variable '#' expr '\n'	{ setear_valor($1, buscar_valor($1) + $3);
                                          printf("%c asignado con %d\n-> ", $1, buscar_valor($1) ); }
        | variable '@' expr '\n'        { setear_valor($1, buscar_valor($1) - $3);
                                          printf("%c asignado con %d\n-> ", $1, buscar_valor($1) ); }
	;
expr	: expr '+' termino		{ $$ = $1 + $3; }
	| expr '-' termino		{ $$ = $1 - $3; }
	| termino
	;
termino	: termino '*' factor		{ $$ = $1 * $3; }
	| termino '/' factor		{ $$ = $1 / $3; }
	| factor
	;
factor	: condic			{ $$ = $1; }
	| '(' expr ')'			{ $$ = $2; }
	| numero
	| variable			{ $$ = buscar_valor($1); }
	;

condic  : '(' expr ')''?' expr ':' expr	{ $$ = ( $2 != 0 )? $5 : $7; } 
	;

numero  : numero digito			{ $$ = $1 * 10 + $2; }
	| digito			{ $$ = $1; }
	;

digito	: '0'				{ $$ = 0; }
	| '1'				{ $$ = 1; }
	| '2'				{ $$ = 2; }
	| '3'				{ $$ = 3; }
	| '4'				{ $$ = 4; }
	| '5'				{ $$ = 5; }
	| '6'				{ $$ = 6; }
	| '7'				{ $$ = 7; }
	| '8'				{ $$ = 8; }
	| '9'				{ $$ = 9; }
	;

variable: 'a'				{ $$ = 'a'; }
	| 'b'				{ $$ = 'b'; }
	| 'c'				{ $$ = 'c'; }
	;

%%
yylex() {
        int c;
        while ( (c=getchar()) == ' ') ;
        return(c);
}

yyerror() {
	printf("Error sintactico\n");
}

main() {
	printf ("-> ");
	yyparse();
}

void setear_valor(char c, int valor) {
	tabla[c - 'a'] = valor;
}

int buscar_valor(char c) {
	return(tabla[c - 'a']);
}
