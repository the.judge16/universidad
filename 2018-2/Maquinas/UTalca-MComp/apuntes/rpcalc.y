
/* Calculadora de notacion polaca inversa de doble presicion
   Ejemplo No 1 del manual de Bison Version 1.27 en espa~nol*/

%{ 
#define YYSTYPE double  /* definicion del tipo de datos de bison a double */
#include <math.h> /* incluyendo las librerias necesarias para las reglas*/
                    /* y las funciones definidas por el usuario*/
#include <stdio.h> 
%} 
%token NUM /* declaracion de un token Bison, esto es un terminal de una GLC */

%% /* A continuacion las reglas gramaticales y las acciones */ 
input:    /* vacioo */ 
        | input line 
; 
line:     '\n' 
        | exp '\n'  { printf ("\t%.10g\n>", $1); }
          /* imprime el resultado del calculo, por eso no incluye un $$ */
; 
exp:      NUM             { $$ = $1;         } 
        | exp exp '+'     { $$ = $1 + $2;    } 
        | exp exp '-'     { $$ = $1 - $2;    } 
        | exp exp '*'     { $$ = $1 * $2;    } 
        | exp exp '/'     { $$ = $1 / $2;    } 
      /* Exponenciación */ 
        | exp exp '^'     { $$ = pow ($1, $2); } 
      /* Menos unario   */ 
        | exp 'n'         { $$ = -$1;        } 
; 
%% /* a continuacion las funciones auxiliares */

/* El analizador lexico devuelve un número n punto
   flotante (double) en la pila y el token NUM, o el 
   caracter ASCII leido si no es un número.  Ignora 
   todos los espacios en blanco y tabuladores, 
   devuelve 0 como EOF.

   El valor de retorno de yylex es el codigo numerico del
   tipo de token. El valor semantico del token se guarda
   en la variable yylval, ver procesamiento de numeros */

#include <ctype.h> /* estas librerias son usadas por las funciones
   yylex, yyerror y main, y las otras funciones definidias por el usuario */

yylex () 
{ 
  int c; 
  /* ignora los espacios en blanco o tabuladores */ 
  while ((c = getchar ()) == ' ' || c == '\t')  
    ; 
  /* procesa numeros */
  if (c == '.' || isdigit (c)) 
    { 
      ungetc (c, stdin); 
      scanf ("%lf", &yylval); 
      return NUM; 
    } 
  /* devuelve fin-de-fichero  */ 
  if (c == EOF)                            
    return 0; 
  /* devuelve caracteres sencillos */ 
  return c;                                
}


/* rutina de manejo de errores */

yyerror (s)  /* Llamada por yyparse ante un error */ 
     char *s; 
{ 
  printf ("%s\n", s); 
}

/* rutina de control */
main() {
  printf("> ");
  yyparse();
}
