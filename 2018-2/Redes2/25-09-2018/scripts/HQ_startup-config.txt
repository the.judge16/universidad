!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HQ
!
!
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
no ip domain-lookup
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 192.168.1.1 255.255.255.192
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 ip address 192.168.1.65 255.255.255.192
!
interface Serial0/0/1
 ip address 209.165.200.226 255.255.255.252
 clock rate 2000000
!
interface Serial0/1/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/1/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/2/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/2/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/3/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/3/1
 no ip address
 clock rate 2000000
 shutdown
!
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router rip
 network 192.168.1.0
!
ip classless
ip route 209.165.202.128 255.255.255.224 209.165.200.225 
!
ip flow-export version 9
!
!
!
banner motd 
********************************
**********     HQ      *********
********************************

!
!
!
!
line con 0
 exec-timeout 15 0
 password cisco
 logging synchronous
 login
!
line aux 0
!
line vty 0 4
 exec-timeout 15 0
 password cisco
 logging synchronous
 login
!
!
!
end

