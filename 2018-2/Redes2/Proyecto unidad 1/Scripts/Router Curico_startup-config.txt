!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname CURICO
!
!
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
!
!
!
ip dhcp pool VLAN108
 network 192.168.10.0 255.255.255.0
 default-router 192.168.10.1
ip dhcp pool VLAN110
 network 192.168.11.0 255.255.255.0
 default-router 192.168.11.1
!
!
!
no ip cef
no ipv6 cef
!
!
!
username cisco password 0 cisco
!
!
!
!
!
!
!
!
no ip domain-lookup
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.1
 encapsulation dot1Q 1 native
 ip address 192.168.20.254 255.255.255.0
!
interface FastEthernet0/0.108
 encapsulation dot1Q 108
 ip address 192.168.10.1 255.255.255.0
!
interface FastEthernet0/0.110
 encapsulation dot1Q 110
 ip address 192.168.11.1 255.255.255.0
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 ip address 200.75.51.130 255.255.255.252
 clock rate 64000
!
interface Serial0/0/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router rip
 network 192.168.10.0
 network 192.168.11.0
 network 192.168.20.0
 network 200.75.51.0
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
line con 0
 password cisco
 login local
!
line aux 0
!
line vty 0 4
 password cisco
 login local
line vty 5 9
 password cisco
 login local
!
!
!
end

